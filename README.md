# Reconcile

pyReconcile is a library providing a reconciliation loop system for enabling declarative configuration of arbitrary systems.

You bring the systems, the state representations, and the actions to modify them, and this brings the glue to put it all together.

## Installaion

## Usage
See two-lists.py for an example of a built around mutating one list into another.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[Mozilla Public License, v2.0](http://mozilla.org/MPL/2.0/)