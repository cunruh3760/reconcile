# -*- coding: utf-8 -*-
#   This Source Code Form is subject to the terms of the Mozilla Public
#   License, v. 2.0. If a copy of the MPL was not distributed with this
#   file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""Expose key configs to the larger module, letting this submodule deal with the details of the exposure."""

from .logging import ENABLE_DEVELOPMENT_LOGGING
