# -*- coding: utf-8 -*-
#   This Source Code Form is subject to the terms of the Mozilla Public
#   License, v. 2.0. If a copy of the MPL was not distributed with this
#   file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""This module showcases an example use of the library, to reconcile two different python Lists."""

# from reconcile.mixins.Action import ActionMixin
# from reconcile.mixins.Plan import PlanMixin
# from reconcile.mixins.DeclarativeState import DeclarativeStateMixin
# from reconcile.mixins.Context import ContextMixin
# from reconcile.Scheduler import Scheduler

import reconcile

from typing import List


class Listly(reconcile.DeclarativeStateMixin):
    """A DeclarativeState representing a python List."""

    def __init__(self, data: List, **kwargs):
        """A DeclarativeState representing a python List.

        Args:
            data (List): The List data we're representing.
        """
        super().__init__(declarative_state_data=data, **kwargs)


class ListlyContext(reconcile.ContextMixin):
    """A Context collecting two Listly DeclarativeStates, a "current" one and a "desired" one."""

    def __init__(self, **kwargs):
        """A Context collecting two Listly DeclarativeStates, a "current" one and a "desired" one.

        Nothing specific about a Listly DeclarativeState that isn't already taken care of by the ContextMixin.
        We just pass ContextMixin kwargs in our construction later, and the Mixin takes care of things.
        """
        super().__init__(**kwargs)


class AddItem(reconcile.ActionMixin):
    """An action that Adds an Item to the Listly in context.current_state.

    We're mutating the context's current_state becasue that's what we want to do.
    Here, you could imagine hitting some REST APIs instead, fetching an updated current_state as part
    of your Plan's create call. This is very extensible.
    """

    def __init__(self, toAdd, **kwargs):
        """Here we take in an additional argument to configure our Action for a later call to "do".

        The super call will pass on any extra kwargs, but here we consume what's relevant to this action.

        We also set the action_name in our call to super().__init__, since that will log later.

        Args:
            toAdd ([type]): Value to add to the list.
        """
        self.toAdd = toAdd
        super().__init__(action_name="Add Item: " + str(toAdd), **kwargs)

    def do(self, context):
        """Add the item (from the constructor) to the given context's current_state."""
        context.current_state.state.append(self.toAdd)


class RemoveItem(reconcile.ActionMixin):
    """An action that removes an item from a list. See AddItem for additional info."""

    def __init__(self, index: int, **kwargs):
        """Here we take the index we're later going to pop off of the Listly's list.

        Args:
            index (int): the list index of the item to later be removed.
        """
        self.index = index
        super().__init__(action_name="Remove Item at index: " + str(index), **kwargs)

    def do(self, context):
        """Pop the item at the already-configured index from the context's current_state's Listly list."""
        context.current_state.state.pop(self.index)


class ChangeItem(reconcile.ActionMixin):
    """An action that changes the item at the given list index to the given value."""

    def __init__(self, index, value, **kwargs):
        """Here we take the index and value we'll use later. See AddItem for additional info.

        Args:
            index (int): Which item to change.
            value: What to change it to.
        """
        self.index = index
        self.value = value
        super().__init__(
            action_name="Change Item: [{}] to {}".format(index, value), **kwargs
        )

    def do(self, context):
        """Set the context's current_state's item per our preconfiguration."""
        context.current_state.state[self.index] = self.value


class ListlyReconciliationPlan(reconcile.PlanMixin):
    """The Plan used to create Actions to mutate the current Listly into the desired one."""

    def __init__(self, **kwargs):
        """Nothing particular needs doing here that isn't already taken care of by PlanMixin's constructor."""
        super().__init__(plan_name="Listly Reconciliation Plan", **kwargs)

    def create(self, context):
        """Create an action plan to reconcile the current Listly into the desired one.

        This is the meat of our loop. Here, we take the output from the context's DeepDiff of the two DeclarativeStates
        and we generate a list of Actions based on what the DeepDiff's differences are reported to be.

        A good understanding of DeepDiff will help here, but for getting started, set up some example contexts and print
        the context.get_state_diff() calls. This will show you the DeepDiffs returned and let you build an appropriate
        process here.

        We always want to ensure that, for some DeepDiff (like an empty one {}), we return no actions, or an empty list of them.

        This is a fairly simple example, but when there are "values_changed", we add ChangeItem actions. When there are
        "iterable_item_added"'s, we add AddItem actions. When there are "iterable_item_removed"'s, we add RemoveItem actions.
        There's some simple parsing done here to extract the data from the returned DeepDiff, like extracting the index. There
        might be a better way to do this, but the proof of concept's core is here.

        Args:
            context (ContextMixin): The context needing to be reconciled.

        Returns:
            List[ActionMixin]: The list of actions to reconcile the context's states.
        """
        diffs = context.get_state_diff()
        actions = []
        if "values_changed" in diffs:
            for value_changed in diffs["values_changed"]:
                # This is a hacky way to chop out the index, probably want something better in prod, should write a helper function probably
                index = int(value_changed[5:-1])
                value = diffs["values_changed"][value_changed]["new_value"]
                actions.append(ChangeItem(index, value))
        if "iterable_item_added" in diffs:
            for iterable_item_added in diffs["iterable_item_added"]:
                value = diffs["iterable_item_added"][iterable_item_added]
                actions.append(AddItem(value))
        if "iterable_item_removed" in diffs:
            for iterable_item_removed in diffs["iterable_item_removed"]:
                index = int(iterable_item_removed[5:-1])
                actions.append(RemoveItem(index))
        return actions


if __name__ == "__main__":

    # logging.getLogger("reconcile.Scheduler").handlers.clear()
    # logging.getLogger("reconcile.Scheduler").addHandler
    reconcile.ENABLE_DEVELOPMENT_LOGGING()

    desired_list = Listly([1, 2, 3], declarative_state_ignore_order=False)
    current_list = Listly([3, 2, 1, 0], declarative_state_ignore_order=False)
    context = ListlyContext(desired_state=desired_list, current_state=current_list)
    plan = ListlyReconciliationPlan()

    print(context.desired_state.state)
    print(context.current_state.state)
    scheduler = reconcile.Scheduler("Name", None)
    scheduler.execute(plan, context)
    print(context.desired_state.state)
    print(context.current_state.state)
